// Projects: Example | Checkout example in nodejs

// File description: Checkout flow to pay request orders.

// Author: mumana@greenpay.me

// Versions:
//     2018-08-09 :   Firts release @greenpay/checkput_nodejs.
//    2018-12-21  :  Add checkout with token 


// Required libs

//Import enviroment var's

//See .env.default and configure it with the credentials provided by Greenpay support team
require('dotenv').config();

//Import nodeJs packages
const rsa = require("node-jsencrypt");
const rsa_ = new rsa();
const aesjs = require("aes-js");
const unirest = require("unirest");
const KJUR = require("jsrsasign");


function generateAESPairs() {
    var key = [];
    var iv;
    for (let k = 0; k < 16; k++) {
        key.push(Math.floor(Math.random() * 255));
    }
    for (let k = 0; k < 16; k++) {
        iv = Math.floor(Math.random() * 255);
    }

    return {
        k: key,
        s: iv
    };
}

//Creates an JSON object with the card data and AES key Pair encrypted
function pack(obj, session, pair_) {
    var pair = (pair_ !== undefined) ? pair_ : generateAESPairs();

    var textBytes = aesjs.utils.utf8.toBytes(JSON.stringify(obj));
    var aesCtr = new aesjs.ModeOfOperation.ctr(pair.k, new aesjs.Counter(pair.s));
    var encryptedBytes = aesCtr.encrypt(textBytes);
    var encryptedHex = aesjs.utils.hex.fromBytes(encryptedBytes);

    var returnObj = {
        session: session,
        ld: encryptedHex,
        lk: rsa_.encrypt(JSON.stringify(pair))
    };

    console.log("pack", returnObj);

    return returnObj;
}

//Pays the order creates in Greenpay gateway
function postCheckout(data, accessToken) {
    return new Promise(function (resolve, reject) {
        unirest.post(process.env.CHECKOUT_ENDPOINT)
            .headers({
                "Accept": "application/json",
                "Content-Type": "application/json",
                "liszt-token": accessToken
            })
            .send(data)
            .end(function (response) {
                if (response.status === 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            });
    });
}

//Creates an payment order in Greenpay Gateway
function postCreateOrder(data) {
    return new Promise(function (resolve, reject) {
        unirest.post(process.env.MERCHANT_ENDPOINT)
            .headers({
                "Accept": "application/json",
                "Content-Type": "application/json",
            })
            .send(JSON.stringify(data))
            .end(function (response) {
                if (response.status === 200) {
                    resolve(response.body);
                } else {
                    reject(response.body);
                }
            });
    });
}

const payOrder = async (paymentData, security) => {
    let resp = {}
    try {
        rsa_.setPublicKey(process.env.PUBLIC_KEY);
        const body = pack(paymentData, security.session);

        console.log(paymentData);
        console.log("liszt :", JSON.stringify(body));
        resp = await postCheckout(body, security.token);
        console.log(JSON.stringify(resp));
    } catch (err) {
        resp = {
            "status": 500
        };
        resp.errors = [err];
        console.error("payOrder", JSON.stringify(resp, null, 2));
    }
    return resp;
}

const verify = (signed, toVerify) => {
    const sig = new KJUR.crypto.Signature({
        "alg": "SHA256withRSA"
    })
    // You should verify with your public key in the PEM format
    sig.init(`-----BEGIN PUBLIC KEY-----${process.env.PUBLIC_KEY}-----END PUBLIC KEY-----`)
    sig.updateString(toVerify)
    return sig.verify(signed)
}

//Main function
handler = async (paymentData, orderRequestData) => {

    let security = await postCreateOrder(orderRequestData); //Create an order in greenpay systema. Receive an object {session: "xxx",token:"xxx"}
    console.log("security", JSON.stringify(security));
    let resp = {}

    try {

        resp = await payOrder(paymentData, JSON.parse(JSON.stringify(security))) //Pay order before created
        const toVerify = `status:${resp.status},orderId:${resp.orderId}`
        const verified = verify(resp._signature, toVerify) //Verify signature returned in payOrder() response.
        if (verified) {
            resp = {
                status: 200,
                description: "Successful"
            }
        } else {
            throw Error("Response was not received from greenpay!")
        }

    } catch (err) {
        resp = {
            "status": 500
        }
        respx.errors = [err]
    }
    return resp
}

/*
    * Use this variable if you want to do a checkout with tokens created in tokenization process
const cardData = {
    "token": "your card token"
};
*/

//Car data
const cardData = {
    "card": {
        "cardHolder": "Jhon Doe",
        "expirationDate": {
            "month": 9,
            "year": 21
        },
        "cardNumber": "4485970366452449", //4485970366452449 DENEGADA, 4777777777777777 OK
        "cvc": "123",
        "nickname": "visa2449"
    }
};

//Order data
const orderRequestData = {
    "secret": process.env.SECRET,
    "merchantId": process.env.MERCHANT_ID,
    "terminal": process.env.TERMINAL_ID,
    "amount": 9425,
    "currency": process.env.CURRENCY,
    "description": "test_Plugins Dev 2",
    "orderReference": "test_Plugins Dev 2"
}

//Call to main fuction
handler(cardData, orderRequestData);

//Prove if .env file is created and configurated
console.log(process.env.CHECKOUT_ENDPOINT);